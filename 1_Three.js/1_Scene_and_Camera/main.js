function init() {
	var scene = new THREE.Scene();

	//////////2////////////
	// var box = getBox(1, 1, 1);
	// box.position.y = box.geometry.parameters.height/2;
	// scene.add(box);
	//////////////////////
	
	//////////3////////////
	// var plane = getPlane(4);
	// plane.rotation.x = Math.PI/2;
	// scene.add(plane);
	//////////////////////

	var camera = new THREE.PerspectiveCamera(
		45,
		window.innerWidth/window.innerHeight,
		1,
		1000
	);
	
	//////////1////////////
	// camera.position.x = 1;
	// camera.position.y = 2;
	// camera.position.z = 5;
	//////////////////////

	camera.lookAt(new THREE.Vector3(0, 0, 0));

	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.getElementById('webgl').appendChild(renderer.domElement);
	renderer.render(
		scene,
		camera 
	);
}

//////////2////////////
// function getBox(w, h, d) {
// 	var geometry = new THREE.BoxGeometry(w, h, d);
// 	var material = new THREE.MeshBasicMaterial({
// 		color: 0x00ff00
// 	});
// 	var mesh = new THREE.Mesh(
// 		geometry,
// 		material 
// 	);

// 	return mesh;
// }
//////////////////////

/////////3/////////////
// function getPlane(size) {
// 	var geometry = new THREE.PlaneGeometry(size, size);
// 	var material = new THREE.MeshBasicMaterial({
// 		color: 0xff0000,
// 		side: THREE.DoubleSide
// 	});
// 	var mesh = new THREE.Mesh(
// 		geometry,
// 		material 
// 	);

// 	return mesh;
// }
//////////////////////

init();